import { useHeaderData } from '@libs/common/react-hooks';
import { HeaderOption } from '@libs/server-side/types';
import React, { FC } from 'react';
import { CommonHeader } from './CommonHeader';

interface CommonTmplProps {}

const CommonTmpl: FC<CommonTmplProps> = (props) => {
    const headerData = useHeaderData<HeaderOption>();
    return (
        <>
            <CommonHeader
                logo={headerData.sloganLogo}
                menus={[
                    // {
                    //     name: 'Dome',
                    //     type: 'customize',
                    //     customizeFC: (
                    //         <img
                    //             src={headerData?.sloganLogo}
                    //             alt="Dome"
                    //             style={{ display: 'inline-block', backgroundColor: 'bisque', height: '100%' }}
                    //         />
                    //     ),
                    // },
                    { name: '工具', active: true },
                    {
                        name: 'zhge ',
                        type: 'customize',
                        customizeFC: <p>asdasdasdad</p>,
                    },
                ]}
            />
            <div>{props?.children}</div>
        </>
    );
};

CommonTmpl.displayName = 'CommonTmpl';

export { CommonTmpl, CommonTmplProps };

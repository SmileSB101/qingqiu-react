import React, { FC } from 'react';

import { MenuItemProps, Menus } from '@libs/elk-ui';

if (process.env.BROWSER) {
    require('../static/styles/common_header.scss');
}

interface CommonHeaderProps {
    logo: string;
    menus: MenuItemProps[];
}

const CommonHeader: FC<CommonHeaderProps> = (props) => {
    return (
        <div className="common_header">
            <div className="header_container">
                <div className="header_content_left">
                    {props?.logo?.length > 0 && (
                        <div className="header_logo content_fl">
                            <img src={props.logo} alt="Home" />
                        </div>
                    )}
                    <Menus data={props.menus} />
                </div>
                <div className="header_content_middle">middle</div>
                <div className="header_content_right">right</div>
            </div>
        </div>
    );
};

CommonHeader.displayName = 'CommonHeader';

export { CommonHeader, CommonHeaderProps };

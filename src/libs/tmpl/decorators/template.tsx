import React from 'react';

function Template(Wrapper):any {
    return (component: any) => {
        class WrapperComponent extends component {
            render() {
                return <Wrapper>{React.createElement(component, this.props)}</Wrapper>;
            }
        }
        return WrapperComponent;
    };
}

export { Template };

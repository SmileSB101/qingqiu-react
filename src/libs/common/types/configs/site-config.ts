interface SiteConfig {
    domain: {
        [key: string]: string;
    };

    staticPath: string;
    favicon: string;
    headerLogo: string;
}

export { SiteConfig };

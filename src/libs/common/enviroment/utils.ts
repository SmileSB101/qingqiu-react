function buildTrimedUpperCaseKey(key: string) {
    return key?.toString()?.trim()?.toUpperCase();
}

function setEnviromentValue(key: string, value: string | undefined | null) {

    const trimedUpperCaseKey = buildTrimedUpperCaseKey(key);

    if ((trimedUpperCaseKey?.length ?? -1) <= 0) {
        return;
    }

    process.env[trimedUpperCaseKey] = value;
}

function getEnivromentValue(key: string): string {
    return process.env?.[buildTrimedUpperCaseKey(key)];
}

export { setEnviromentValue, getEnivromentValue };
enum EnvironmentParameters {
    RUN_ENV = "RUN_ENV",
    ROOT_PATH = "ROOT_PATH",
    ROOT_STATIC_RESOURCE_PATH = "ROOT_STATIC_RESOURCE_PATH"
};

enum EnviromentType {
    dev = 'dev',
    // Pre = 'Pre',
    prd = 'prd'
}

export { EnvironmentParameters, EnviromentType };
interface ProjectSetting {
    rootPath: string;
    publishPath?: string;
    serverPath?: string;
}

interface ConfigSetting {
    path: string;

    type: 'json' | 'xml';
}

interface ConfigFileSettings {
    [key: string]: ConfigSetting;
}

interface RemoteConfigSetting {
    enable: boolean;

    remoteAddress: string;
    remoteBasicPath: string;
}

interface AppSettings {
    projectSetting: ProjectSetting;

    remoteConfig?: RemoteConfigSetting;

    configs: ConfigFileSettings;
}

interface DomainSetting {
    domain: string;
}

export { AppSettings, DomainSetting };

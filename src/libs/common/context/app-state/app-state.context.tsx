import React, { createContext, FC } from 'react';
import { AppState } from './app-state.types';

export const AppStateContext = createContext<AppState>({ initData: {} });

export const AppStateProvider: FC<{ values: AppState }> = (props) => {
    return (
        <AppStateContext.Provider value={props?.values}>
            <>{props?.children}</>
        </AppStateContext.Provider>
    );
};

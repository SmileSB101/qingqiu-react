import path from "path";

import { EnvironmentParameters, EnviromentType, getEnivromentValue, AppSettings } from "../enviroment";
import { getConfig } from "./utils";

export class ConfigCenter {
    private readonly env: EnviromentType;

    private readonly rootPath: string;

    private readonly configPath: string;

    private readonly appsettings: AppSettings;

    constructor() {
        this.env = getEnivromentValue(EnvironmentParameters.RUN_ENV) === EnviromentType.prd ? EnviromentType.prd : EnviromentType.dev;
        this.rootPath = getEnivromentValue(EnvironmentParameters.ROOT_PATH) || '';
        this.configPath = path.join(this.rootPath, './conf');

        // appsettings (only load local config)
        this.appsettings = getConfig<AppSettings>("appsettings.json", this.configPath, this.env);
    }

    public getAppSettings(): AppSettings {
        return this.appsettings;
    }

    public getConfig<T = any>(name: string): T {

        const trimedName = name?.toString()?.trim();

        return getConfig(
            `${trimedName}${trimedName?.endsWith(".json") ? "" : ".json"}`,
            this.configPath,
            this.env
        )
    }

}
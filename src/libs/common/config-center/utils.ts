import { readJsonSync, existsSync } from 'fs-extra';
import path from "path";

import { EnviromentType } from "../enviroment";

function mergeConfig(fromConfig: any, toConfig: any) {
    let newConfig = { ...fromConfig };

    if (!!fromConfig && !!toConfig) {
        // override all match config without check it's depth
        Object.keys(toConfig)?.forEach((toKey) => {
            newConfig[toKey] = toConfig?.[toKey];
        })
    }

    return newConfig;
}

function getConfig<T = any>(name: string, specificPath: string, env: EnviromentType) {
    try {
        const baseFile = readJsonSync(path.join(specificPath, name));
        const commonConfig = baseFile?.default ?? baseFile;
        const additionalFilePath = path.join(specificPath, env, name);

        if (additionalFilePath?.length > 0 && existsSync(additionalFilePath)) {
            const additionalFile = readJsonSync(additionalFilePath);

            const additionalConfig = additionalFile?.default ?? additionalFile;

            return mergeConfig(commonConfig, additionalConfig) as T;
        }

        return commonConfig as T;
    }
    catch (err) {
        console.log('get config:', name, 'Error:', err);
        return {} as T;
    }
}

export { getConfig };
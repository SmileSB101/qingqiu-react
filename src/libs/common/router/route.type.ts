import { Domain } from './route.enum';

interface RouteOption {
    page: string;
    params?: { [key: string]: string };
}

interface PathMapping {
    [key: string]: { path: string; domain: Domain };
}

type Protocol = 'http' | 'https';

export { Protocol, PathMapping, RouteOption };

import { Domain, PageAlias } from './route.enum';
import { PathMapping } from './route.type';

const PagePathMapping: PathMapping = {
    [PageAlias.DemonMInfo]: { path: 'demon/minfo', domain: Domain.MainSite },
    [PageAlias.HomeHome]: { path: 'home', domain: Domain.MainSite },
};

export { PagePathMapping };

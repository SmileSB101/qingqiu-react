export enum Domain {
    MainSite = 'MainSite',
}

export enum PageAlias {
    HomeHome = 'HomeHome',
    HomeApp = 'HomeApp',
    // demon module
    DemonMInfo = 'DemonMInfo',
}

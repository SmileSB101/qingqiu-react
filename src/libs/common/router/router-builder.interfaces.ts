import { Protocol } from './route.type';

export interface IRouterBuilder {
    setProtocol(protocol: Protocol): IRouterBuilder;
    setPath(path: string): IRouterBuilder;
    build(parameters: { [key: string]: string | boolean | number }): string;
}

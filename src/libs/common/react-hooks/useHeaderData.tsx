import React, { useContext } from 'react';

import { AppStateContext } from '../context';
// import { AllAppStateContext } from '@libs/common';

export const useHeaderData = <T,>(): T => useContext(AppStateContext)?.headerData;

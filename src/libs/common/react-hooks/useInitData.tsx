import React, { useContext } from 'react';

import { AppStateContext } from '../context';
// import { AllAppStateContext } from '@libs/common';

export const useInitData = <T,>(): T => useContext(AppStateContext)?.initData;

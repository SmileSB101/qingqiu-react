import { Injectable, Scope } from '@nestjs/common';
import { ConfigCenter } from '../config-center';
import { AbstractHttpClientFactory } from './abstract-http-client-factory';
import { HttpRequest } from './http-request';
import { IRestClient } from './http.type';

@Injectable({ scope: Scope.REQUEST })
export class NodeHttpClientFactory extends AbstractHttpClientFactory {
    constructor(private configCenter: ConfigCenter) {
        super();
    }

    public createResquest(apiName: string): IRestClient {
        const apiConfig = this.configCenter.getConfig('test');

        // const request = HttpRequest.create();

        // eslint-disable-next-line unicorn/no-null
        return null;
    }
}

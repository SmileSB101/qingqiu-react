/* eslint-disable no-underscore-dangle */
import React, { ComponentType } from 'react';
import { hydrate } from 'react-dom';

import { AppStateProvider } from '@libs/common/context';

const isWindowHere = typeof window !== 'undefined' && window !== null;
const initData = isWindowHere && (window as any)?.__initData__;
const headerData = isWindowHere && (window as any)?.__headerData__;

function bootstrapReact(rootElement: ComponentType, rootSelector = '#app') {
    const RootElement = rootElement;

    hydrate(
        <AppStateProvider values={{ initData, headerData }}>
            <RootElement />
        </AppStateProvider>,
        document.querySelector(rootSelector),
    );
}

export { bootstrapReact };

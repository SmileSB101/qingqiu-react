import React, { FC, useState } from 'react';

import { MenuItem, MenuItemProps } from './MenuItem';

if (process.env.BROWSER) {
    require('../static/styles/menu.scss');
}

interface MenusProps {
    data: MenuItemProps[];
}

const Menus: FC<MenusProps> = (props) => {
    const [activeIndex, setActiveIndex] = useState(props?.data?.findIndex((d) => !!d.active) ?? 0);

    return (
        props.data?.length > 0 && (
            <ul className="elk__toolbar-menus content_fl">
                {props.data?.map((d, i) => {
                    const item = { ...d, active: activeIndex === i };
                    return <MenuItem key={`${d.name}_${i}`} {...item} onClick={() => setActiveIndex(i)} />;
                })}
            </ul>
        )
    );
};

Menus.displayName = 'Menus';

export { Menus, MenusProps };

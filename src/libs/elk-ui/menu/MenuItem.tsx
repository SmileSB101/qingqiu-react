import React, { FC } from 'react';
import cn from 'classnames';
import { isFunction } from 'lodash';

interface MenuItemProps {
    name: string;

    active?: boolean;
    type?: 'text' | 'link' | 'img' | 'customize';
    customizeFC?: JSX.Element; // we'll use this when type is customize
    onClick?: () => void;
}

const MenuItem: FC<MenuItemProps> = (props) => {
    const { type = 'text' } = props;
    return (
        <li
            className={cn({ active: !!props.active })}
            data-name={props.name}
            onClick={() => {
                if (isFunction(props.onClick)) {
                    props.onClick();
                }
            }}
        >
            {/* type is text */}
            {/* {type === 'text' && <Link props.name?.trim()/>} */}
            {/* type is customize */}
            {type === 'customize' && props.customizeFC}
        </li>
    );
};

MenuItem.displayName = 'MenuItem';

export { MenuItem, MenuItemProps };

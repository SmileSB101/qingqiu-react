import { ModuleMetadata } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { NestFastifyApplication, FastifyAdapter } from "@nestjs/platform-fastify";
import fastify from "fastify";
import metricsPlugin from "fastify-metrics";

import { EnvironmentParameters, EnviromentType, getEnivromentValue, setEnviromentValue } from "@libs/common/enviroment";

import { BootstrapOptions } from "./bootstrap.interfaces";
import { ConfigCenter } from "@libs/common/config-center";

export default async function bootstrap(module: ModuleMetadata, options?: BootstrapOptions) {
    const fastifyInstance = fastify({ ignoreTrailingSlash: true, caseSensitive: false });

    fastifyInstance.register(metricsPlugin, { endpoint: "/metrics" });

    fastifyInstance.addHook("onSend", (request, reply, payload, done) => {
        reply.header('s-site-id', 'default');
        done(undefined, payload);
    });

    const isProd = getEnivromentValue('NODE_ENV') === EnviromentType.prd;

    setEnviromentValue(EnvironmentParameters.RUN_ENV, isProd ? EnviromentType.prd : EnviromentType.dev);
    setEnviromentValue(EnvironmentParameters.ROOT_PATH, options?.rootDir);

    // must call when set RUN_ENV & ROOT_PATH
    // const configCenter = new ConfigCenter();

    const app = await NestFactory.create<NestFastifyApplication>(module, new FastifyAdapter());

    const port = options?.port ?? 8231;

    await app.listen(port,(err,address)=>{
        console.log('env:', getEnivromentValue(EnvironmentParameters.RUN_ENV));
        console.log('rootPath:', getEnivromentValue(EnvironmentParameters.ROOT_PATH));

        console.log(`server are running on ${address} now.`);
    });
}
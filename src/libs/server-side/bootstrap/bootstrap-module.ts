import { Module, OnModuleInit, Global } from "@nestjs/common"
import { ServerSideConfigService } from "../services";

declare let global: any;

@Global()
@Module({
    providers: [ServerSideConfigService],
    controllers: [], //faq
    exports: [ServerSideConfigService]
})
export default class BootstrapModule implements OnModuleInit {
    root!: string;

    onModuleInit() {
        this.root = "bootstrap module";
        // global.root = 'bootstrap module';
    }

}
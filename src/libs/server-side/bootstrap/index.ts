import bootstrap from "./bootstrap";

import BootstrapModuleFactory from "./bootstrap-module-factory";
export { bootstrap, BootstrapModuleFactory };
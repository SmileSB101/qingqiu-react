import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FastifyReply } from 'fastify';

import { METADATA } from '@libs/common/reflects';

import { ControllerMethod, RenderOption } from '../../types';
import { ServerSideConfigService } from '../../services';
import { createHtml, ssrErrorRender } from '../../render';

@Injectable()
export class RenderReactInterceptor implements NestInterceptor {
    constructor(private configCenter: ServerSideConfigService) {}

    intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
        const element = (context.getHandler() as ControllerMethod).reactRootElement;
        if (!element) {
            return next.handle();
        }

        const request = context.switchToHttp().getRequest();
        const reply = context.switchToHttp().getResponse() as FastifyReply;
        const pageName = Reflect.getMetadata(METADATA.PAGE_NAME_METADATA, context.getHandler());

        const options: RenderOption = {
            getConfig: (configName: string) => this.configCenter.get(configName),
            rootElement: element,
            pageName,
            request,
            reply,
        };

        // set to be html
        reply.header('Content-Type', 'text/html;charset=UTF-8');

        return createHtml(next.handle(), options).pipe(
            catchError((error) => of(ssrErrorRender({ request, reply, exception: error }))),
        );
    }
}

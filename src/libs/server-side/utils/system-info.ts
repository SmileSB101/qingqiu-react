import os from 'os';

import { SystemInfo } from '../types';

function getSystemInfo(): SystemInfo {
    return {
        hostName: os.hostname(),
        arch: os.arch(),
        platform: os.platform(),
        systemType: os.release(),
        systemVersion: os.version(),
        loadAvg: os.loadavg(),
        liveTime: os.uptime(),
        cpus: os.cpus(),
        totalMem: os.totalmem(),
        freeMem: os.freemem(),
        networks: os.networkInterfaces(),
    };
}

export { getSystemInfo };

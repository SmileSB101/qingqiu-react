import { CpuInfo, NetworkInterfaceInfo } from 'os';

interface SystemInfo {
    hostName: string;
    arch: string;
    platform: NodeJS.Platform;
    systemType: string;
    systemVersion: string;
    loadAvg: number[];
    liveTime: number;
    cpus: CpuInfo[];
    totalMem: number;
    freeMem: number;
    networks: NodeJS.Dict<NetworkInterfaceInfo[]>;
}

export { SystemInfo };

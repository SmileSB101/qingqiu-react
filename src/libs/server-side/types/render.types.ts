import { FastifyReply, FastifyRequest } from 'fastify';
import { ComponentType } from 'react';

import { HtmlScriptProps, HtmlStyleProps, WindowScriptProps } from '../render/html-tags';

interface MetaOption {
    name: string;
    value: string;
}

interface RobotsOption {
    enable: boolean;
}

interface SEOOption {
    title?: string;
    keywords?: string;
    description?: string;

    robots?: RobotsOption;
}

interface HeaderOption {
    icon?: string;
    sloganLogo?: string;
    staticPath?: string;
}

interface RenderOption {
    rootElement: ComponentType;
    pageName: string;
    request: FastifyRequest<any>;
    reply: FastifyReply<any>;

    getConfig: <T = any>(name: string) => T;
}

// use this when rendered
interface RenderModel {
    controllerReply: any;

    renderOption: RenderOption;

    metas: Array<MetaOption>;

    windowScripts: Array<WindowScriptProps>;

    injectedScripts: Array<HtmlScriptProps>;
    injectedStyles: Array<HtmlStyleProps>;

    headerOption?: HeaderOption;
}

type ControllerMethod = Function & {
    reactRootElement: ComponentType;
};

export { ControllerMethod, HeaderOption, RenderOption, RenderModel, MetaOption, RobotsOption, SEOOption };

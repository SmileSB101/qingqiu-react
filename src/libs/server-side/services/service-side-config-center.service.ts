import { Injectable } from '@nestjs/common';

import { ConfigCenter } from '@libs/common/config-center';

@Injectable()
export class ServerSideConfigService {
    private readonly configCenter: ConfigCenter;

    constructor() {
        // dependency on RUN_ENV & ROOT_PATH
        this.configCenter = new ConfigCenter();
    }

    public get(configName: string): any {
        return this.configCenter.getConfig(configName);
    }
}

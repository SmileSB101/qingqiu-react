import { ComponentType } from 'react';

import { METADATA } from "@libs/common/reflects"

import { ControllerMethod } from '../types';

function ReactRootView(rootElement: ComponentType, pageName: string) {
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        // console.log('shishi:', Reflect.getMetadata(PATH_METADATA, descriptor.value));
        Reflect.defineMetadata(METADATA.PAGE_NAME_METADATA, pageName, descriptor.value);
        (descriptor.value as ControllerMethod).reactRootElement = rootElement;
    };
}

export { ReactRootView };
import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { FastifyReply, FastifyRequest } from 'fastify';

import { ErrorArgument } from '../../types';
import { ServerSideRenderError, ssrErrorRender } from '../../render';

@Catch()
export class CommonExceptionFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost) {
        const context = host.switchToHttp();
        const request = context.getRequest() as FastifyRequest;
        const reply = context.getResponse() as FastifyReply;

        const args: ErrorArgument = { exception, request, reply };

        if (exception instanceof ServerSideRenderError) {
            ssrErrorRender(args);
        }
    }
}

import React, { ComponentType, FC } from 'react';

import { AppStateProvider } from '@libs/common/context';

import { HeaderOption } from '../../types';
import { WindowScriptProps } from '../html-tags';
import HtmlBody from './HtmlBody';
import { HeadProps, HtmlHead } from './HtmlHeader';

interface HtmlStructureProps {
    bodyElement: ComponentType;
    headOption: HeadProps;

    headerData?: HeaderOption;
    initData: any;

    windowScripts?: WindowScriptProps[];
}

const HtmlStructure: FC<HtmlStructureProps> = (props) => {
    const BodyEl = props.bodyElement;

    return (
        <AppStateProvider values={{ initData: props.initData, headerData: props.headerData }}>
            <html lang="zh">
                <HtmlHead {...props.headOption} />
                <HtmlBody.HtmlBody
                    initData={props?.initData}
                    headerData={props.headerData}
                    windowScripts={props.windowScripts}
                >
                    <BodyEl data-content />
                </HtmlBody.HtmlBody>
            </html>
        </AppStateProvider>
    );
};

HtmlStructure.displayName = 'HtmlStructure';

export { HtmlStructure };

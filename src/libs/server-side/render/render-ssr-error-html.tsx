import { FastifyReply } from 'fastify';
import React, { ComponentType } from 'react';
import { renderToString } from 'react-dom/server';
import { ErrorArgument } from '../types';
import { HtmlSSRError } from './errors';

const html5DocumentType = '<!DOCTYPE html>';

function renderString<T = any>(Inner: ComponentType, props: T) {
    return html5DocumentType + renderToString(<Inner {...props} />);
}

function errorRender(reply: FastifyReply, statusCode: number, htmlString: string) {
    return reply.status(statusCode).header('Content-Type', 'text/html;charset=UTF-8').send(htmlString);
}

function ssrErrorRender(error: ErrorArgument) {
    return errorRender(error.reply, 500, renderString(HtmlSSRError, { error: error.exception }));
}

export { ssrErrorRender };

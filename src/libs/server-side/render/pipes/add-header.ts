import { getConfig } from '@libs/common/config-center';
import { SiteConfig } from '@libs/common/types/configs';
import { RenderModel } from '@libs/server-side/types';
import { trimEnd } from 'lodash';

function addHeader(result: RenderModel) {
    const siteConfig = result?.renderOption?.getConfig<SiteConfig>('site-config');

    const host = trimEnd(siteConfig?.staticPath, '/');

    result.headerOption = {
        staticPath: host,
        icon: `${host}/assets/${siteConfig.favicon}`,
        sloganLogo: `${host}/assets/${siteConfig.headerLogo}`,
    };

    return result;
}

export { addHeader };

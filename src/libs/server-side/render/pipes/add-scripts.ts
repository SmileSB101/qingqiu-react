import { isArray } from 'lodash';

import { RenderModel } from '@libs/server-side/types';
import { SiteConfig } from '@libs/common/types/configs';

import { WindowScriptProps } from '../html-tags';


const addScripts = (result: RenderModel): RenderModel => {
    const scriptsMapping = result.renderOption.getConfig('resources.mapping');
    result.injectedScripts = [];

    Object.keys(scriptsMapping)
        .filter((key) => [result.renderOption.pageName].includes(key))
        .forEach((key) => {
            const script = scriptsMapping[key];

            if (isArray(script?.js)) {
                // link script
                script.js.forEach((j: string) => {
                    result.injectedScripts.push({
                        src: j,
                        type: 'linkScript',
                        props: { defer: !!j?.includes(result.renderOption?.pageName) }, // if is this page,we need defer to load.
                    });
                });
            } else {
                // inner script
                result.injectedScripts.push({ innerScript: script.js, type: 'innerScript' });
            }
        });

    // add window scripts
    const windowScripts: WindowScriptProps[] = [];
    const siteConfig = result.renderOption.getConfig<SiteConfig>('site-config');
    // eslint-disable-next-line no-unused-expressions
    !!siteConfig && windowScripts.push({ propertyName: 'SITE_CONFIG', content: siteConfig });

    result.windowScripts = windowScripts;

    return result;
};

export { addScripts };
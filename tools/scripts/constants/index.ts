import ProjectConfig from './project.constants';
import ServerConfig from './server.contansts';
import WebpackConfig from './webpack.contansts';

export { ProjectConfig, ServerConfig, WebpackConfig };

export default { ProjectConfig, ServerConfig, WebpackConfig };

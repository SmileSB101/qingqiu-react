pipeline {
  agent any
  stages {
    stage('Prepare env') {
      steps {
        script {
          env.TIMESTRAP = sh(returnStdout: true, script: 'date +%Y%m%d%H%M%S').trim()
          env.DOCKER_TAG = "${env.GIT_COMMIT_SHORT}_T${env.TIMESTRAP}_BI${env.CI_BUILD_ID}"
        }

        sh 'printenv'
      }
    }
    stage('Get Code') {
      steps {
        checkout([
          $class: 'GitSCM',
          branches: [[name: env.GIT_BUILD_REF]],
          userRemoteConfigs: [[
            url: env.GIT_REPO_URL,
            credentialsId: env.CREDENTIALS_ID
          ]]])
        }
      }
      stage('Install Pakcages') {
        agent {
          docker {
            reuseNode true
            registryUrl 'https://coding-public-docker.pkg.coding.net'
            image 'public/docker/nodejs:14'
          }

        }
        steps {
          sh 'yarn install'
        }
      }
      stage('Build') {
        agent {
          docker {
            reuseNode 'true'
            registryUrl 'https://coding-public-docker.pkg.coding.net'
            image 'public/docker/nodejs:14'
          }

        }
        steps {
          sh 'yarn release'
        }
      }
      stage('Commit Static Resource') {
        steps {
          sh 'git --version'
          dir('dist/git/') {
            sh 'ls'
            checkout([
              $class: 'GitSCM',
              branches: [[name: '*/master']],
              userRemoteConfigs: [[
                url: 'https://gitee.com/SmileSB101/qing-qiu-react-static.git',
                credentialsId: env.CREDENTIALS_ID
              ]]])
              sh 'ls'
              withCredentials([ usernamePassword(credentialsId: env.CREDENTIALS_ID,
              usernameVariable: "GIT_USERNAME", 
              passwordVariable: "GIT_PASSWORD")
            ]) {
              sh 'git config --local credential.helper "!p() { echo username=\\$GIT_USERNAME; echo password=\\$GIT_PASSWORD; }; p"'
              sh "git branch -a | egrep 'remotes/origin/${env.STATIC_GIT_BRANCH}' && (git checkout ${env.STATIC_GIT_BRANCH} && git pull) || (git branch '${env.STATIC_GIT_BRANCH}' && git push origin '${env.STATIC_GIT_BRANCH}')"
              sh '''
                cp ../publish/static . -vur 
                '''
                
              sh """
              git add .
              git branch -a

              git config --global user.email 'zxb13194889618@outlook.com'
              git config --global user.name 'SmileSB101'

              git status | egrep 'nothing to commit, working tree clean' && echo 'not need commit this time.' || (git add . && git commit -m 'Add ${env.DOCKER_TAG} static changes,hash is ${env.GIT_COMMIT}' && git push origin HEAD:'${env.STATIC_GIT_BRANCH}')
              """
            }

          }

        }
      }
      stage('Build Docker & Push Image') {
        agent {
          docker {
            reuseNode true
            registryUrl 'https://coding-public-docker.pkg.coding.net'
            image 'public/docker/nodejs:14'
          }
        }
        steps {
          dir('dist/publish/server/') {
            echo 'New Tag is: ${env.DOCKER_TAG}'
            sh 'docker -v'
            sh "echo '${env.DOCKER_PWD}' | docker login '${env.DOCKER_REGISTRY}' -u='${env.DOCKER_USER_NAME}' --password-stdin"
            sh "docker build -t ${env.DOCKER_IMAGE_NAME}:${env.DOCKER_TAG} ."
            sh "docker push ${env.DOCKER_IMAGE_NAME}:${env.DOCKER_TAG}"
          }

        }
      }
    }
  }
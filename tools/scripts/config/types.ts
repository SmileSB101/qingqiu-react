export enum ModuleRoot {
    home = './src/modules/home',
    demon = './src/modules/demon',
}

export interface PageMapping {
    [key: string]: string;
}

export interface PageMappingConfig {
    name: string;
    path: string;
}

export interface PageResource {
    vendors?: boolean;
    [key: string]: boolean;
}

export interface PageResourceMapping {
    [key: string]: { scripts?: PageResource; styles?: PageResource };
}

import path from 'path';
import webpack from 'webpack';
import argv from 'yargs';
import { PageMapping, PageResourceMapping, ResourceNotBuildInMapping } from '../config';
import getConfig from '../utils/config.util';

import { getCssLoaders } from '../utils/webpack.util';
import AllConst, { WebpackConfig } from '../constants';

import CopyPlugin from 'copy-webpack-plugin';
import WebpackBar from 'webpackbar';

import TerserPlugin from 'terser-webpack-plugin';
import OptimizeCssAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import CircularDependencyPlugin from 'circular-dependency-plugin';
import AssetsPlugin from 'assets-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

const { PROJECT_PATH, STATIC_PATH_DEV, STATIC_PATH_PRD } = AllConst.ProjectConfig;

function getEntry(): PageMapping {
    const pageConfig = getConfig('page.entry.ts', './tools/scripts/config') as PageMapping;

    const pageMapping: PageMapping = {};
    Object.keys(pageConfig).forEach((key) => {
        const value = pageConfig[key];

        if (value?.trim()?.length >= 0) {
            pageMapping[key] = path.resolve(PROJECT_PATH, pageConfig[key]);
        }
    });

    return pageMapping;
}

const chunks = getEntry();

export function getCommonWebpackConfig(isDev: boolean) {
    const conf: webpack.Configuration = {
        target: 'web',
        mode: 'production',
        entry: chunks,
        module: {
            rules: [
                {
                    test: /\.s[ac]ss$/,
                    use: [
                        ...getCssLoaders(2, isDev),
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: isDev,
                                // use dart-css
                                implementation: require('sass'),
                            },
                        },
                    ],
                },
                {
                    test: /\.less$/,
                    use: [
                        ...getCssLoaders(2, isDev),
                        {
                            loader: 'less-loader',
                            options: {
                                sourceMap: isDev,
                            },
                        },
                    ],
                },
                {
                    test: /\.css$/,
                    use: getCssLoaders(1, isDev),
                },
                {
                    test: /\.(bmp|gif|jpg|jpeg|png|ico)$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 10 * 1024,
                                name: '[name].[contenthash:8].[ext]',
                                outputPath: 'assets/images',
                            },
                        },
                    ],
                },
                {
                    test: /\.(ttf|woff|woof2|eot|otf)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[contenthash:8].[ext]',
                                outputPath: 'assets/fonts',
                            },
                        },
                    ],
                },
                {
                    test: /\.(tsx|js|jsx)$/,
                    loader: 'babel-loader',
                    options: { cacheDirectory: true },
                    exclude: /node_modules/,
                },
                {
                    test: /\.ts?$/,
                    use: 'ts-loader',
                    exclude: /node_modules/,
                },
            ],
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js', '.json'],
            alias: {
                '@libs': path.resolve(PROJECT_PATH, './src/libs'),
                '@static': path.resolve(PROJECT_PATH, './static'),
                '@config': path.resolve(PROJECT_PATH, './config'),
                '@': path.resolve(PROJECT_PATH, './src'),
            },
        },
        output: {
            path: path.resolve(PROJECT_PATH, `./dist/${isDev ? 'development' : 'publish'}/static`),
            filename: `scripts/[name].${isDev ? '' : '[contenthash].'}js`,
        },
        plugins: [
            ...WebpackConfig.fixedPlugins,
            new MiniCssExtractPlugin({
                filename: 'styles/[name].[contenthash:8].css',
                chunkFilename: 'styles/[name].[contenthash:8].css',
                ignoreOrder: false,
            }),
            new CopyPlugin({
                patterns: [
                    {
                        context: path.resolve(PROJECT_PATH, './static/assets'),
                        from: '*',
                        to: path.resolve(PROJECT_PATH, `./dist/${isDev ? 'development' : 'publish'}/static/assets`),
                        toType: 'dir',
                    },
                ],
                // copyUnmodified: true,
            }),
            new WebpackBar({
                name: isDev ? 'Starting' : 'Packaging',
                color: '#fa8c16',
            }),
            new CircularDependencyPlugin({
                exclude: /node_modules/,
                include: /src/,
                failOnError: true,
                allowAsyncCycles: false,
                cwd: process.cwd(),
            }),
            new AssetsPlugin({
                removeFullPathAutoPrefix: true,
                path: path.resolve(PROJECT_PATH, `./dist/${isDev ? 'development' : 'publish'}/conf`),
                filename: 'resources.mapping.json',
                processOutput: function (mapping) {
                    console.log('build.........resources');
                    console.log(mapping);

                    const scripts = {};

                    const devPath = STATIC_PATH_DEV?.endsWith('/') ? STATIC_PATH_DEV : STATIC_PATH_DEV + '/';
                    const prdPath = STATIC_PATH_PRD?.endsWith('/') ? STATIC_PATH_PRD : STATIC_PATH_PRD + '/';
                    const scriptPath = isDev ? devPath : prdPath;

                    const pageResourceConfig = getConfig(
                        'page.resource.ts',
                        './tools/scripts/config',
                    ) as PageResourceMapping;
                    for (let key in mapping) {
                        if (!ResourceNotBuildInMapping.includes(key) && !!mapping[key]?.js) {
                            const resources = pageResourceConfig[key]?.scripts;
                            const currentScripts: string[] = [];
                            if (mapping[key]?.js?.length > 0) {
                                // push main resource into currentScripts
                                currentScripts.push(`${scriptPath}${mapping[key]?.js}`);
                            }

                            // push other special script into currentScripts
                            for (let resource in resources) {
                                if (!!mapping[resource]?.js) {
                                    currentScripts.push(`${scriptPath}${mapping[resource]?.js}`);
                                }
                            }

                            const currentStyles: string[] = [];
                            if (mapping[key]?.css?.length > 0) {
                                // push main resource into currentScripts
                                currentStyles.push(`${scriptPath}${mapping[key]?.css}`);
                            }

                            // push other special script into currentScripts
                            for (let resource in resources) {
                                if (!!mapping[resource]?.css) {
                                    currentStyles.push(`${scriptPath}${mapping[resource]?.css}`);
                                }
                            }

                            scripts[key] = { js: currentScripts, css: currentStyles };
                        }
                    }

                    console.log('SSSSSSSSSSSSSSSSSSSS');
                    console.log(scripts);

                    return `${JSON.stringify(scripts, null, 2)}`;
                },
            }),
        ],
        optimization: {
            minimize: !isDev,
            minimizer: [
                !isDev &&
                    new TerserPlugin({
                        extractComments: false,
                        terserOptions: {
                            compress: { pure_funcs: ['console.log'] },
                        },
                    }),
                !isDev && new OptimizeCssAssetsPlugin(),
            ].filter(Boolean),

            splitChunks: {
                chunks: 'initial',
                minSize: 20000,
                minChunks: 1,
                maxAsyncRequests: 7,
                maxInitialRequests: 4,
                //name: true,
                automaticNameDelimiter: '~',
                cacheGroups: {
                    vendors: {
                        test: /[/\\]node_modules[/\\]/,
                        minChunks: 2,
                        priority: -5,
                        name: 'vendors',
                    },
                    default: {
                        minChunks: 2,
                        priority: -20,
                        reuseExistingChunk: true,
                    },
                },
            },
        },
    };

    return conf;
}

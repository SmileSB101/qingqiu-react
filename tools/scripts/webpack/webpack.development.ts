import { merge } from 'webpack-merge';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';

import { getCommonWebpackConfig } from './webpack.common';
import { UnionWebpackConfigWithDevelopmentServer } from '../types';

function getDevelopmentWebpackConfig():UnionWebpackConfigWithDevelopmentServer {
    const commonConfig = getCommonWebpackConfig(true);

    return merge(commonConfig, {
        devtool: 'eval-source-map',
        mode: 'development',
        stats: {
            errorDetails: true,
        },
        // output: {
        //     path: path.resolve(PROJECT_PATH, `./dist/development/static`),
        //     filename: `scripts/[name].js`,
        // },
        plugins: [
            new CleanWebpackPlugin(),

            // new HardSourceWebpackPlugin(), //only use in dev environment
            // new HardSourceWebpackPlugin.ExcludeModulePlugin([
            //     {
            //         test: /mini-css-extract-plugin[\\/]dist[\\/]loader/,
            //     },
            // ]),
        ],
    });
}

export default getDevelopmentWebpackConfig();

import merge from 'webpack-merge';
import webpack from 'webpack';

import { getCommonWebpackConfig } from './webpack.common';
import { UnionWebpackConfigWithDevelopmentServer } from '../types';
import { WebpackConfig } from '../constants';

function getProductionWebpackConfig(): UnionWebpackConfigWithDevelopmentServer {
    const commonConfig = getCommonWebpackConfig(false);

    return merge(commonConfig, {
        // devtool: 'none',
        mode: 'production',
        plugins: [
            ...WebpackConfig.fixedPlugins,
            new webpack.BannerPlugin({
                raw: true,
                banner: '/** @preserve Powered by react-ts-quick-starter (https://github.com/vortesnail/react-ts-quick-starter) */',
            }),
            // new BundleAnalyzerPlugin({
            //     // analyzerMode: "", // 开一个本地服务查看报告
            //     // analyzerHost: 'localhost', // host 设置
            //     // analyzerPort: 9628, // 端口号设置
            //     analyzerMode: 'static', // static mode
            //     reportFilename: './dist/analyzer/report.html',
            // }),
        ],
    });
}

export default getProductionWebpackConfig();

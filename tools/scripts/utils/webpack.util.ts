import { RuleSetUseItem } from "webpack";
import MiniCssExtractPlugin from "mini-css-extract-plugin";

function getCssLoaders(importLoaders: number, isDev: boolean): RuleSetUseItem[] {
    return [MiniCssExtractPlugin.loader,
    {
        loader: 'css-loader',
        options: {
            modules: false,
            sourceMap: isDev,
            importLoaders
        }
    },
    {
        loader: 'postcss-loader',
        options: {
            sourceMap: isDev
        }
    }]
}

export { getCssLoaders };
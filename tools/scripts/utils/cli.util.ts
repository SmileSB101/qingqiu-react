const getEnvironmentValue = (parameter: string) => process.env[parameter];

export default { getEnvironmentValue };
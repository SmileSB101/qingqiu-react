import { SiteConfig } from '../src/libs/common/types/configs';

const siteConfig: SiteConfig = {
    domain: {
        mainSite: 'main site',
    },
    staticPath: 'http://localhost:9627',
    favicon: 'favicon.ico',
    headerLogo: 'dome-slogan.png',
};

export default siteConfig;

import path from 'path';
import { AppSettings } from '../src/libs/common/enviroment';

const appsetting: AppSettings = {
    projectSetting: {
        rootPath: path.join(__dirname, '..'),
        publishPath: 'dist',
        serverPath: 'server',
    },
    configs: {
        common: {
            path: 'CommonConfig',
            type: 'json',
        },
    },
};

export default appsetting;

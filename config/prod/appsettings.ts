import path from 'path';
import { AppSettings } from '../../src/libs/common/enviroment/app-settings.type';

const appsetting: AppSettings = {
    projectSetting: {
        rootPath: path.join(__dirname, '..'),
        publishPath: 'dist',
        serverPath: 'server',
    },
    configs: {
        common: {
            path: 'CommonConfig',
            type: 'json',
        },
    },
};

export default appsetting;
